from flask    import jsonify, request
from config   import Config

def auth_credentials_provided():
    return request.authorization

def check_auth(username, password):
    return username == 'key' and password == Config.CDN_KEY

def authenticated():
    auth = auth_credentials_provided()
    if not auth:
        return False
    return check_auth(auth.username, auth.password)

def auth_required_error():
    message = {
        'message': "Requires Authentication",
        'status': 401,
    }
    return jsonify(message), 401

def auth_credentials_error():
    message = {
        'message': "Authentication failed. API credentials are incorrect",
        'status': 401,
    }
    return jsonify(message), 401
