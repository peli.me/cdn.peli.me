#!/bin/sh
set -e #exit on error

mongodb_id="$(docker ps | awk '/mongo-auth/{print $1;exit;}')"
mongo="docker exec -i ${mongodb_id} mongo app -u app -p app"

$mongo --eval 'db.resource.find();'

read -p "Continue (Y/n)? " choice
case "${choice}" in
  n|N) exit;;
esac

$mongo --eval 'db.resource.drop();'
$mongo --eval 'db.resource.find();'
rm -rf /cdn/*.part
