from app            import app
from app.models     import Notification
from app.mail       import send_email

from flask          import render_template
from config         import Config
from hurry.filesize import size
from datetime       import datetime, timedelta

def notify_storage_threshold(total, used, free, usage_percentage, cdn_root_path):
    if usage_percentage > Config.CDN_STORAGE_THRESHOLD:
        notification_msg = "Warning usage_percentage {}%".format(Config.CDN_STORAGE_THRESHOLD)
        notification = Notification.objects(msg=notification_msg).first()

        if notification:
            if not (datetime.utcnow() - notification.date) > timedelta(1):
                #less than 24 hrs passed
                # notification.delete()
                return

        msg  = "WARNING: {} is above threshold!!!\n".format(cdn_root_path)
        msg += "usage_percentage: {:.2f}%, threshold: {}%\n".format(usage_percentage, Config.CDN_STORAGE_THRESHOLD)
        msg += "total: {}, used: {}, free: {}".format(size(total), size(used), size(free))
        app.logger.debug(msg)

        if Config.CDN_THRESHOLD_WARNING_MAILS:
            stats = {
                'total':size(total), 'used':size(used), 'free':size(free),
                'threshold': Config.CDN_STORAGE_THRESHOLD,
                'usage_percentage': int(usage_percentage), 'cdn_root_path':cdn_root_path
            }
            send_email(
                "WARNING {}: storage {} is above {}% / current {}%".format(
                    Config.CDN_DOMAIN, cdn_root_path, Config.CDN_STORAGE_THRESHOLD, usage_percentage
                    ),
               Config.APP_ADMIN,
               render_template("storage_threshold_email.txt.j2",  stats=stats),
               render_template("storage_threshold_email.html.j2", stats=stats)
            )

        if notification: notification.delete()
        new_notification = Notification(msg=notification_msg).save()
