from app import db
from datetime import datetime

class Resource(db.Document):
    encoded_uri = db.StringField(required=True, unique=True)
    uri         = db.StringField(required=True, unique=True)

    status      = db.StringField(required=True, choices=(
                    'STARTING',
                    'IN-PROGRESS',
                    'DONE',
                  ))

class Notification(db.Document):
    msg  = db.StringField(required=True)
    date = db.DateTimeField(default=datetime.now)
