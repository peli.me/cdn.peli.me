About
-----

[cdn.peli.me](https://cdn.peli.me/)

Usage
-----

    #config
    $ echo 'CDN_DOMAIN=cdn.peli.me'  > .env.dev #.env.prod
    $ echo 'CDN_KEY=default'        >> .env.dev #.env.prod
    $ echo 'CDN_HOSTS=cdn.peli.me,a.cdn.peli.me,b.cdn.peli.me,c.cdn.peli.me' >> .env.dev #.env.prod

    #single box development
    $ ./setup.sh [docker-compose-file]

    #multi box development
    $ vagrant up
    $ ANSIBLE_ARGS="--tags upload" vagrant provision

    #production
    $ docker run --rm -it -p 127.0.0.1:9150:9150 peterdavehello/tor-socks-proxy
    $ ./deploy.sh prod #in another window

    #production fast-forward (only updates app's code)
    $ docker run --rm -it -p 127.0.0.1:9150:9150 peterdavehello/tor-socks-proxy
    $ ./deploy-fast-forward.sh prod #in another window

### Sample interactions

    # GET RESOURCE, NO AUTHENTICATION
    $ curl -L -k -i cdn.peli.me/<encoded_uri>
      HTTP/1.0 200 OK

    # CACHE RESOURCE, AUTHENTICATION REQUIRED
    $ curl -L -k -i -u "key:default" cdn.peli.me/<encoded_uri>
      HTTP/1.0 200 OK

    # REMOVE RESOURCE, AUTHENTICATION REQUIRED
    $ curl -L -k -i -u "key:default" -X DELETE cdn.peli.me/<encoded_uri>
      HTTP/1.0 200 OK

Dependencies
------------

- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- [vagrant](https://www.vagrantup.com/)
- [ansible](https://www.ansible.com/)
