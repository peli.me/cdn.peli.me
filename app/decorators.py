from threading import Thread
from functools import wraps
from flask     import request

from app.auth  import check_auth, auth_required_error

def threaded(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return auth_required_error()

        elif not check_auth(auth.username, auth.password):
            return auth_required_error()
        return f(*args, **kwargs)

    return decorated
