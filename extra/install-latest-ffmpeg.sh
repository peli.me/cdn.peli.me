#!/bin/sh
VERSION="1.2.1"
URL="https://www.johnvansickle.com/ffmpeg/"
URL_RELEASES="https://johnvansickle.com/ffmpeg/releases/"
URL_BUILDS="https://johnvansickle.com/ffmpeg/builds/"
TMP_DIR="/tmp"
PATH_INSTALL="/usr/bin/"

if [ "$(whoami)" != "root" ] ; then
    printf "%s\\n" "You must be root!"
    exit 1
fi

if [ "$(uname -s)" == "Linux" ]; then
    KERNEL="linux"
else
    printf "%s\\n" "Unsupported OS (${KERNEL})"
    exit 1
fi

case "$(uname -m)" in
      i?86) ARCH="i686" ;;
    x86_64) ARCH="amd64" ;;
         *) printf "%s\\n" "Unsupported Arquitecture (${archs})" && exit 1 ;;
esac

case "${1}" in

   --install|-install|--update|-update)
     cd "${TMP_DIR}"
     rm -rf ffmpeg-*
     if [ "${2}" == "release" ] ; then
         curl "${URL}" | grep "${URL_RELEASES}" | cut -d '"' -f 2 | grep "${ARCH}" | head -1 > "${TMP_DIR}/ffmpeg-url"
         if [ "${?}" != "0" ]; then
             printf "%s\\n" "Connection problem!"
             printf "%s\\n" "Exiting..."
             exit 1
         fi
     else
        curl "${URL}" | grep "${URL_BUILDS}" | cut -d '"' -f 2 | grep "${ARCH}" | head -1 > "${TMP_DIR}/ffmpeg-url"
        if [ "${?}" != "0" ] ; then
            printf "%s\\n" "Connection problem!"
            printf "%s\\n" "Exiting..."
            exit 1
        fi
     fi

     URL_PACKAGE="$(cat "${TMP_DIR}/ffmpeg-url")"
     NAME_PACKAGE="$(cat /tmp/ffmpeg-url | cut -d "/" -f 6)"

     printf "%s\\n" "Downloading ${NAME_PACKAGE}"

     curl "${URL_PACKAGE}" -o "${NAME_PACKAGE}"
     if [ "$?" != "0" ] ; then
        printf "%s\\n" "Connection problem!"
        printf "%s\\n" "Exiting..."
        exit 1
     fi
     tar Jxvf "${NAME_PACKAGE}"
     rm -f ffmpeg-url ffmpeg*xz
     cd ffmpeg-*
     cp -rf ffmpeg  "${PATH_INSTALL}"
     cp -rf ffprobe "${PATH_INSTALL}"
     cd ..
     rm -rf ffmpeg-*
     printf "%s\\n" "Done!"
     exit 0
   ;;

   --uninstall|-uninstall)
      printf "%s\\n" "Uninstalling..."
      sleep 3
      rm -rf /usr/bin/ffmpeg
      rm -rf /usr/bin/ffprobe
      rm -rf /usr/bin/ffserver
      rm -rf /usr/bin/ffmpeg-10bit
      printf "%s\\n" "Done!"
   ;;

   --help|-help|-h|*)
      printf "\\n"
      printf "%s\\n" "** ffmpeg-install v.${VERSION} **"
      printf "\\n"
      printf "%s\\n" "* How to install:"
      printf "\\n"
      printf "%s\\n" "  ffmpeg-install --install (Latest git version)"
      printf "%s\\n" "  ffmpeg-install --install release (Latest stable version)"
      printf "\\n"
      printf "%s\\n" "* How to update:"
      printf "\\n"
      printf "%s\\n" "  ffmpeg-install --update (Latest git version)"
      printf "%s\\n" "  ffmpeg-install --update release (Latest stable version)"
      printf "\\n"
      printf "%s\\n" "* How to uninstall:"
      printf "\\n"
      printf "%s\\n" "  ffmpeg-install --uninstall"
      printf "\\n"
      printf "%s\\n" "* Show help:"
      printf "\\n"
      printf "%s\\n" "  ffmpeg --help"
      printf "\\n"
      exit 0
esac
