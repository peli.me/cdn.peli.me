import zlib, urllib, re

from app.encode import b64_encode, b64_decode

def encode_uri(uri):
    encoded_uri = b64_encode(zlib.compress(uri.encode(),9))
    encoded_uri = urllib.parse.quote_plus(encoded_uri.decode())
    return encoded_uri

def decode_uri(encoded_uri):
    if re.search("^magnet:", encoded_uri) or re.search("^http(s)?://", encoded_uri):
        uri = encoded_uri
    else:
        encoded_uri = urllib.parse.unquote_plus(encoded_uri)
        uri         = zlib.decompress(b64_decode(encoded_uri)).decode()
    return uri
