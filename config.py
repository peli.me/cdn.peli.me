import os
import urllib.parse

class Config(object):
    #general settings
    CDN_DOMAIN = os.environ.get('CDN_DOMAIN') or 'cdn.peli.me'
    CDN_KEY    = os.environ.get('CDN_KEY')    or 'default'
    CDN_HOSTS  = os.environ.get('CDN_HOSTS')  or 'localhost'

    #enable/disable email sending
    APP_MAIL_SENDING  = os.environ.get('APP_MAIL_SENDING') or  True
    APP_MAIL_SENDING  = False if APP_MAIL_SENDING == "no" else True
    APP_MAIL_PROVIDER = os.environ.get('APP_MAIL_PROVIDER') or 'SMTP'

    #threshold warnings
    CDN_THRESHOLD_WARNING_MAILS = os.environ.get('CDN_THRESHOLD_WARNING_MAILS') or False
    CDN_THRESHOLD_WARNING_MAILS = False if CDN_THRESHOLD_WARNING_MAILS == "no" else True
    CDN_STORAGE_THRESHOLD = os.environ.get('CDN_STORAGE_THRESHOLD') or 80
    CDN_STORAGE_THRESHOLD = int(CDN_STORAGE_THRESHOLD)

    APP_FROM   = os.environ.get('APP_FROM')   or 'no-reply@peli.me'
    APP_ADMIN  = os.environ.get('APP_ADMIN')  or 'admin@peli.me'

    SMTP_SERVER   = os.environ.get('SMTP_SERVER') or 'localhost'
    SMTP_PORT     = os.environ.get('SMTP_PORT')   or 25
    SMTP_PORT     = int(SMTP_PORT)
    SMTP_USERNAME = os.environ.get('SMTP_USERNAME') or 'guest'
    SMTP_PASSWORD = os.environ.get('SMTP_PASSWORD') or 'you-will-never-guess'
    SMTP_USE_TLS  = os.environ.get('SMTP_USE_TLS')  or 'yes'

    #flask-mongoengine
    MONGODB_HOST     = os.environ.get('MONGODB_HOST')     or 'mongodb'
    MONGODB_TCP_PORT = os.environ.get('MONGODB_TCP_PORT') or 27017
    MONGODB_TCP_PORT = int(MONGODB_TCP_PORT)
    MONGODB_DB       = os.environ.get('MONGODB_DB')       or 'app'
    MONGODB_USER     = os.environ.get('MONGODB_USER')     or 'app'
    MONGODB_PASSWD   = os.environ.get('MONGODB_PASSWD')   or 'app'

    MONGODB_USER     = urllib.parse.quote_plus(MONGODB_USER)
    MONGODB_PASSWD   = urllib.parse.quote_plus(MONGODB_PASSWD)

    MONGODB_SETTINGS = {
        'db':       MONGODB_DB,
        'host':     MONGODB_HOST,
        'port':     MONGODB_TCP_PORT,
        'username': MONGODB_USER,
        'password': MONGODB_PASSWD,
    }

    MAILGUN_API    = os.environ.get('MAILGUN_API')    or 'you-will-never-guess'
    MAILGUN_DOMAIN = os.environ.get('MAILGUN_DOMAIN') or CDN_DOMAIN

    #per environment settings
    APP_ENVIRONMENT = os.environ.get('APP_ENVIRONMENT') or 'development'

    if   APP_ENVIRONMENT == 'development':
        DEBUG  = True
    elif APP_ENVIRONMENT == 'production' :
        DEBUG  = False
