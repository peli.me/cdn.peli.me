#!/usr/bin/env python3
import sys
import os
import argparse

import zlib
import urllib.parse

exec(open("../app/encode.py").read())

def parser():
    parser = argparse.ArgumentParser(description="encode uri helper")
    parser.add_argument('URI', nargs=1, help="URI to encode")
    return parser.parse_args()

### copied from ../app/utils.py ########################################
def encode_uri(uri):
    encoded_uri = b64_encode(zlib.compress(uri.encode(),9))
    encoded_uri = urllib.parse.quote_plus(encoded_uri.decode())
    return encoded_uri
########################################################################

def main():
    args = parser()
    uri  = args.URI[0]
    print(encode_uri(uri))

if __name__ == "__main__":
    main()
