from __future__ import unicode_literals

from flask      import Response, jsonify, redirect, request
from random     import shuffle

from config     import Config

from app        import app
from app.utils  import decode_uri
from app.models import Resource
from app.decorators import threaded

import youtube_dl
import requests
import re
import os

def redirect_locally(encoded_uri):
    response = Response()
    # force to create a download link
    # response.headers["Content-Disposition"] = "attachment; filename={0}".format(encoded_uri)
    response.headers["X-Accel-Redirect"] = "/resource/{0}".format(encoded_uri)

    # https://serverfault.com/questions/195060/nginx-x-accel-redirect-and-mime-types
    response.headers["Content-Type"] = "" # let nginx guess the file type

    return response

def redirect_to_cdn(encoded_uri):
    peers = cdn_peers()

    for peer in peers:
        peer_uri = "http://" + peer + "/local/" + encoded_uri

        try:
            peer_request = requests.head(
                               peer_uri,
                               timeout=3,
                               allow_redirects=True,
                               verify=False,
                           )

            if peer_request.ok:
                if peer_request.status_code == 200:
                    return redirect("http://{}/{}".format(peer, encoded_uri), code=302)
                else:
                    return resource_in_progress(decode_uri(encoded_uri))

        except requests.exceptions.Timeout:
            app.logger.debug("{} timeout!".format(peer_uri))

    return resource_404()

@threaded
def threaded_upload_locally(encoded_uri, uri):
    ydl_opts = {
        'format':  'mp4',
        'outtmpl': '/cdn/{}'.format(encoded_uri),
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([uri])
        Resource.objects(encoded_uri=encoded_uri).delete()

def upload_locally(encoded_uri):
    uri = decode_uri(encoded_uri)

    if not resource_supported(uri):
        return resource_invalid(uri)

    if not Resource.objects(encoded_uri=encoded_uri).first():
        new_resource = Resource(
                         encoded_uri=encoded_uri,
                         uri=uri,
                         status='STARTING',
                       ).save()
        threaded_upload_locally(encoded_uri, uri)
    return resource_in_progress(uri)

def upload_to_cdn(encoded_uri):
    peers = cdn_peers()
    peers_sorted_by_free_space = sort_peers_by_free_space(peers)
    # app.logger.debug(peers_sorted_by_free_space)

    for peer in peers_sorted_by_free_space:
        peer_uri = "http://" + peer + "/" + encoded_uri

        try:
            auth = request.authorization
            peer_cache_request = requests.put(
                                    peer_uri,
                                    auth=(auth.username, auth.password),
                                    timeout=3,
                                    allow_redirects=True,
                                    verify=False,
                                 )

            if  peer_cache_request.ok:
                peer_cache_request = peer_cache_request.json()
                return jsonify(peer_cache_request), 201

        except requests.exceptions.Timeout:
                app.logger.debug("{} timeout!".format(peer_uri))

    return resource_404()

def delete_from_cdn(encoded_uri):
    peers = cdn_peers()
    peers_with_content = []

    for peer in peers:
        peer_uri = "http://" + peer + "/local/" + encoded_uri

        try:
            auth = request.authorization
            peer_delete_request = requests.delete(
                                    peer_uri,
                                    auth=(auth.username, auth.password),
                                    timeout=3,
                                    allow_redirects=True,
                                    verify=False,
                                 )

            if  peer_delete_request.ok:
                peers_with_content.append(peer)

        except requests.exceptions.Timeout:
                app.logger.debug("{} timeout!".format(peer_uri))

    if peers_with_content:
        return resource_cdn_deleted(peers_with_content)

    return resource_404()

def delete_from_local(encoded_uri):
    if resource_is_available_locally(encoded_uri):
        os.remove('/cdn/{}'.format(encoded_uri))
    Resource.objects(encoded_uri=encoded_uri).delete()
    uri = decode_uri(encoded_uri)
    return resource_deleted(uri)

def resource_is_available_locally(encoded_uri):
    return os.path.isfile('/cdn/{}'.format(encoded_uri))

def sort_peers_by_free_space(peers):
    peers_stats = {}

    for peer in peers:
        peer_uri = "http://" + peer + "/stats/fs"

        try:
            auth = request.authorization
            peers_stats[peer] = requests.get(
                                    peer_uri,
                                    auth=(auth.username, auth.password),
                                    timeout=3,
                                    allow_redirects=True,
                                    verify=False,
                                ).json()

        except requests.exceptions.Timeout:
            app.logger.debug("{} timeout!".format(peer_uri))

    peers_sorted_by_free_space = {
            k: v for k, v in sorted(
                peers_stats.items(),
                key=lambda json: json[1]['stats']['fs']['free'],
                reverse=True,
                )
            }

    return peers_sorted_by_free_space

def cdn_peers():
    peers = Config.CDN_HOSTS.split(',')
    shuffle(peers) #in-place
    return peers

def resource_supported(uri):
    if re.search("^http(s)?://", uri):
        return True
    else:
        return False

######################################################################

def resource_404():
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

def resource_deleted(uri):
    message = {
        'status': 200,
        'message': 'Deleted: ' + uri,
    }
    resp = jsonify(message)
    resp.status_code = 200
    return resp

def resource_cdn_deleted(peers):
    message = {
        'status': 200,
        'message': 'Deleted: ' + request.url,
        'peers': peers,
    }
    resp = jsonify(message)
    resp.status_code = 200
    return resp

def resource_invalid(uri):
    message = {
        'status': 415,
        'message': 'Unsupported Media Type: ' + uri,
    }
    resp = jsonify(message)
    resp.status_code = 415
    return resp

def resource_in_progress(uri):
    message = {
        'status': 201,
        'message': 'IN PROGRESS: ' + uri,
    }
    resp = jsonify(message)
    resp.status_code = 201
    return resp
