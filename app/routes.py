from flask          import request, jsonify, abort, render_template, url_for, redirect
from shutil         import disk_usage
from hurry.filesize import size

from app            import app
from app.models     import Resource
from app.cdn        import resource_is_available_locally
from app.cdn        import resource_in_progress, resource_404
from app.cdn        import redirect_locally, redirect_to_cdn
from app.cdn        import upload_locally, upload_to_cdn
from app.cdn        import delete_from_cdn, delete_from_local
from app.auth       import auth_credentials_provided, authenticated
from app.auth       import auth_credentials_error
from app.threshold  import notify_storage_threshold
from app.decorators import requires_auth

@app.route('/<encoded_uri>', methods=['GET'])
def get_or_add_resource(encoded_uri):
    if auth_credentials_provided() and not authenticated():
        return auth_credentials_error()

    if resource_is_available_locally(encoded_uri):
        return redirect_locally(encoded_uri)

    if not authenticated():
        return redirect_to_cdn(encoded_uri)

    cdn_request=redirect_to_cdn(encoded_uri)

    if cdn_request.status_code <= 400: #found
        return cdn_request
    else:
        return upload_to_cdn(encoded_uri)

@app.route('/local/<encoded_uri>', methods=['GET'])
def get_local_resource(encoded_uri):
    if resource_is_available_locally(encoded_uri):
        return redirect_locally(encoded_uri)

    resource = Resource.objects(encoded_uri=encoded_uri).first()
    if resource:
        return resource_in_progress(encoded_uri)
    else:
        return resource_404()

# curl -L -k -i -u "key:default" cdn.peli.me/stats/fs
@app.route('/stats/fs', methods=['GET'])
@app.route('/stats/fs/<format>', methods=['GET'])
@requires_auth
def stats_fs(format="machine"):
    cdn_root_path     = "/cdn"
    total, used, free = disk_usage(cdn_root_path)
    usage_percentage  = (used * 100) / total
    notify_storage_threshold(total, used, free, usage_percentage, cdn_root_path)

    if format == "human":
        total = size(total)
        used  = size(used)
        free  = size(free)

    return jsonify(
        {
            'stats': {
                'fs': {
                    'path':  cdn_root_path,
                    'total': total,
                    'used':  used,
                    'free':  free,
                    'usage': '{:.2f}%'.format(usage_percentage),
                }
            }
        }), 201

@app.route('/<encoded_uri>', methods=['PUT'])
@requires_auth
def cache_resource(encoded_uri):
    return upload_locally(encoded_uri)

@app.route('/<encoded_uri>', methods=['DELETE'])
@requires_auth
def delete_resource(encoded_uri):
    return delete_from_cdn(encoded_uri)

@app.route('/local/<encoded_uri>', methods=['DELETE'])
@requires_auth
def delete_local_resource(encoded_uri):
    return delete_from_local(encoded_uri)

######################################################################

@app.route('/ping', methods=['GET'])
def ping():
    message = {
        'status': 200,
        'message': 'Pong!',
    }
    resp = jsonify(message)
    resp.status_code = 200
    return resp

######################################################################

#TODO 17-03-2019 11:08 >> replace this with a proper waf solution
@app.route('/index')
@app.route('/index.html')
@app.route('/index.php')
@app.route('/favicon.ico')
def index_html():
    return redirect(url_for('index'))

@app.route('/')
def index():
    return render_template("index.html.j2")

######################################################################

@app.route('/exception')
def error():
    raise Exception("I'm sorry, Dave. I'm afraid I can't do that.")

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

@app.errorhandler(400)
def not_found(error=None):
    message = {
        'status': 400,
        'message': 'Bad request',
    }
    resp = jsonify(message)
    resp.status_code = 400
    return resp

@app.errorhandler(405)
def not_found(error=None):
    message = {
        'status': 405,
        'message': 'Method not allowed for: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 405
    return resp

@app.errorhandler(500)
def not_found(error=None):
    message = {
        'status': 500,
        'message': 'Internal error',
    }
    resp = jsonify(message)
    resp.status_code = 500
    return resp
